# Logo, Wallpapers and more

Official Social Media (SM) graphics, templates and samples. 

The purpose for this repository is to allow community leaders from all communities around the world to have proper disposal and make every community COMPLIANT to the official and global one. 

Please, use the graphics properly, for example: 
* Ask before use samples that were not used before in our official communities. Maybe they're planned to be used in a specific way. 

* Remember that SM profiles of the national communities must be as compliant as possible with the official global profiles. 

* For the reason above, you should use the provided font on this folder, as in this way we can assure continuity and people can associate you with the official channels. 

* If you're a community leader, and you manage SM, feel free to contact me for any further information, such as the general guidelines for a nice, compliant graphic. 
